"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const task_schema_1 = require("./task.schema");
const getAllTasks = (filter) => task_schema_1.taskModel.find(filter);
const createTask = (task) => __awaiter(void 0, void 0, void 0, function* () {
    yield task_schema_1.taskModel.create(task);
    return true;
});
const getTask = (filter) => task_schema_1.taskModel.findOne(filter);
const updateTask = (filter, updateTaskBody) => __awaiter(void 0, void 0, void 0, function* () {
    yield task_schema_1.taskModel.updateOne(filter, updateTaskBody);
    return true;
});
const editTask = (filter, editTaskBody) => __awaiter(void 0, void 0, void 0, function* () {
    yield task_schema_1.taskModel.updateOne(filter, editTaskBody);
    return true;
});
const deleteTask = (filter) => __awaiter(void 0, void 0, void 0, function* () {
    yield task_schema_1.taskModel.deleteOne(filter);
    return true;
});
exports.default = {
    getAllTasks,
    createTask,
    getTask,
    updateTask,
    editTask,
    deleteTask
};
