import { Itask } from "../feature-modules/task/task.types";

export class ResponseHandler{
    constructor(
        public data : any,
        public error : any = null        
    ){}
}