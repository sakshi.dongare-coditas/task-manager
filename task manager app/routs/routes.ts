import { Application, NextFunction, json ,Request , Response} from "express";
import { routes } from "./routes.data";
import { ResponseHandler } from "../utility/response";


export const resisterRoutes = (app:Application) =>{
    app.use(json());

    for(let router of routes){
        app.use(router.path,router.route);
    }

    app.use((err:any,req:Request,res:Response,next:NextFunction)=>{
        res.send(new ResponseHandler([],err));
    });
}