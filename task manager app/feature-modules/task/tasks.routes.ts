import { Router } from "express";
import { Route } from "../../routs/routes.types";
import taskService from "./task.service";
import { ResponseHandler } from "../../utility/response";
import { Responses } from "./task.response";

const router = Router();

router.post("/addTask", async (req, res, next) => {
     try {
          const result = await taskService.createTask(req.body);
          res.send(new ResponseHandler(result));
     } catch (err) {
          next(err);
     }
});

router.get("/getAllTasks", async (req, res, next) => {
     try {
          /*
          // Extra for query String. 
          const queryEle = req.query;
          console.log(queryEle);
          const [id1] = Object.keys(queryEle);
          console.log(id1);
          console.log(queryEle[id1]);
          */

          const allTask = await taskService.getAllTasks();
         res.send(new ResponseHandler(allTask));
     } catch (err) {
          next(err);
     }
})

router.get("/getTask/:taskId", async (req, res, next) => {
     try {
          const { taskId } = req.params;
          const task = await taskService.getTask(taskId);
          res.send(new ResponseHandler(task));
     } catch (err) {
          next(err);
     }
});

router.put("/updateTask/:taskId", async (req, res, next) => {
     try {
          const { taskId } = req.params;
          const updateTaskBody = req.body;
          let result = await taskService.updateTask(taskId, updateTaskBody);
          res.send(new ResponseHandler(result));
     }
     catch (err) {
          next(err);
     }
});

router.put("/editTask/:taskId", async (req, res, next) => {
     try {
          const { taskId } = req.params;
          const editTask = req.body;
          let result = await taskService.editTask(taskId, editTask);
          res.send(new ResponseHandler(result));
          
     } catch (err) {
          next(err);
     }
});

router.delete("/deleteTask/:taskId", async (req, res, next) => {
     try {
          const { taskId } = req.params;
          let result = await taskService.deleteTask(taskId);
          res.send(new ResponseHandler(result));
     } catch (err) {
          next(err);
     }
});

export default new Route(
     "/taskManager",
     router
);