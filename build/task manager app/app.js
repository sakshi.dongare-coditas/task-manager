"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StartServer = void 0;
const express_1 = __importDefault(require("express"));
const routes_1 = require("./routs/routes");
const mongo_connection_1 = require("./connections/mongo.connection");
const StartServer = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const app = (0, express_1.default)();
        (0, routes_1.resisterRoutes)(app);
        yield (0, mongo_connection_1.connectToMongo)();
        const port = process.env.PORT || 3000;
        app.listen(port, () => console.log(`SERVER IS LISTNING AT PORT ${port} ...`));
    }
    catch (err) {
        console.log("CAN NOT CONNECT TO SERVER ...");
        console.log(err);
    }
});
exports.StartServer = StartServer;
