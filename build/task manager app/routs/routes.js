"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resisterRoutes = void 0;
const express_1 = require("express");
const routes_data_1 = require("./routes.data");
const response_1 = require("../utility/response");
const resisterRoutes = (app) => {
    app.use((0, express_1.json)());
    for (let router of routes_data_1.routes) {
        app.use(router.path, router.route);
    }
    app.use((err, req, res, next) => {
        res.send(new response_1.ResponseHandler([], err));
    });
};
exports.resisterRoutes = resisterRoutes;
