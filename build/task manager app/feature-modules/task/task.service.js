"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTask = exports.editTask = exports.updateTask = exports.getTask = exports.createTask = exports.getAllTasks = void 0;
const task_repo_1 = __importDefault(require("./task.repo"));
const task_response_1 = require("./task.response");
const getAllTasks = () => __awaiter(void 0, void 0, void 0, function* () {
    const allTask = yield task_repo_1.default.getAllTasks({ isDeleted: false });
    return allTask;
});
exports.getAllTasks = getAllTasks;
const createTask = (task) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield task_repo_1.default.createTask(task);
    if (result) {
        return task_response_1.Responses.addTask.Success;
    }
    else {
        throw { msg: task_response_1.Responses.addTask.Fail };
    }
});
exports.createTask = createTask;
const getTask = (taskId) => __awaiter(void 0, void 0, void 0, function* () { return yield task_repo_1.default.getTask({ _id: taskId }); });
exports.getTask = getTask;
const updateTask = (taskId, updateTaskBody) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield task_repo_1.default.updateTask({ _id: taskId }, updateTaskBody);
    if (result) {
        return task_response_1.Responses.updateTask.Success;
    }
    else {
        throw { msg: task_response_1.Responses.updateTask.Fail };
    }
});
exports.updateTask = updateTask;
const editTask = (taskId, editTaskBody) => __awaiter(void 0, void 0, void 0, function* () {
    const Task = yield (0, exports.getTask)(taskId);
    const editedTask = (Task === null || Task === void 0 ? void 0 : Task.task) + " " + editTaskBody.task;
    const result = yield task_repo_1.default.editTask({ _id: taskId }, { task: editedTask });
    if (result) {
        return task_response_1.Responses.editTask.Success;
    }
    else {
        throw { msg: task_response_1.Responses.editTask.Fail };
    }
});
exports.editTask = editTask;
const deleteTask = (taskId) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield task_repo_1.default.deleteTask({ _id: taskId });
    if (result) {
        return task_response_1.Responses.deleteTask.Success;
    }
    else {
        throw { msg: task_response_1.Responses.deleteTask.Fail };
    }
});
exports.deleteTask = deleteTask;
exports.default = {
    getAllTasks: exports.getAllTasks,
    createTask: exports.createTask,
    getTask: exports.getTask,
    updateTask: exports.updateTask,
    editTask: exports.editTask,
    deleteTask: exports.deleteTask
};
