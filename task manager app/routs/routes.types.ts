import { Router } from "express";

export class Route{
    private static Paths : string[] = [];
    constructor(
       public path : string,
       public route : Router
    ){
       if(!path.startsWith("/")){
          throw Error("Path must Starts with '/'");
       }
       if(Route.Paths.includes(path)){
          throw Error("this Path is already exists");
       }
       Route.Paths.push(path);
    }
}

export type Routes = Route[];