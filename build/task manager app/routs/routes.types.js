"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Route = void 0;
class Route {
    constructor(path, route) {
        this.path = path;
        this.route = route;
        if (!path.startsWith("/")) {
            throw Error("Path must Starts with '/'");
        }
        if (Route.Paths.includes(path)) {
            throw Error("this Path is already exists");
        }
        Route.Paths.push(path);
    }
}
Route.Paths = [];
exports.Route = Route;
