import { taskModel } from "./task.schema";
import { Itask, IeditTask } from "./task.types";

const getAllTasks = (filter: Partial<Itask>) => taskModel.find(filter);

const createTask = async (task: Itask) => {
    await taskModel.create(task);
    return true;
};

const getTask = (filter: Partial<Itask>) => taskModel.findOne(filter);

const updateTask = async (filter: Partial<Itask>, updateTaskBody: Partial<Itask>) => {
    await taskModel.updateOne(filter, updateTaskBody);
    return true;
};

const editTask = async (filter: Partial<Itask>, editTaskBody: IeditTask) => {
    await taskModel.updateOne(filter, editTaskBody)
    return true;
};

const deleteTask = async (filter: Partial<Itask>) => {
    await taskModel.deleteOne(filter);
    return true;
};


export default {
    getAllTasks,
    createTask,
    getTask,
    updateTask,
    editTask,
    deleteTask
}