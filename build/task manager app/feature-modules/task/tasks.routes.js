"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const routes_types_1 = require("../../routs/routes.types");
const task_service_1 = __importDefault(require("./task.service"));
const response_1 = require("../../utility/response");
const router = (0, express_1.Router)();
router.post("/addTask", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield task_service_1.default.createTask(req.body);
        res.send(new response_1.ResponseHandler(result));
    }
    catch (err) {
        next(err);
    }
}));
router.get("/getAllTasks", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        /*
        // Extra for query String.
        const queryEle = req.query;
        console.log(queryEle);
        const [id1] = Object.keys(queryEle);
        console.log(id1);
        console.log(queryEle[id1]);
        */
        const allTask = yield task_service_1.default.getAllTasks();
        res.send(new response_1.ResponseHandler(allTask));
    }
    catch (err) {
        next(err);
    }
}));
router.get("/getTask/:taskId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { taskId } = req.params;
        const task = yield task_service_1.default.getTask(taskId);
        res.send(new response_1.ResponseHandler(task));
    }
    catch (err) {
        next(err);
    }
}));
router.put("/updateTask/:taskId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { taskId } = req.params;
        const updateTaskBody = req.body;
        let result = yield task_service_1.default.updateTask(taskId, updateTaskBody);
        res.send(new response_1.ResponseHandler(result));
    }
    catch (err) {
        next(err);
    }
}));
router.put("/editTask/:taskId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { taskId } = req.params;
        const editTask = req.body;
        let result = yield task_service_1.default.editTask(taskId, editTask);
        res.send(new response_1.ResponseHandler(result));
    }
    catch (err) {
        next(err);
    }
}));
router.delete("/deleteTask/:taskId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { taskId } = req.params;
        let result = yield task_service_1.default.deleteTask(taskId);
        res.send(new response_1.ResponseHandler(result));
    }
    catch (err) {
        next(err);
    }
}));
exports.default = new routes_types_1.Route("/taskManager", router);
