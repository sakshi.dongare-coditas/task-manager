"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const routes_types_1 = require("../routs/routes.types");
const router = (0, express_1.Router)();
router.post("/addTask", (req, res, next) => {
    res.send(req.body);
});
exports.default = new routes_types_1.Route("/taskManager", router);
