import { ObjectId } from "mongoose"

export type Itask = {
    _id? : ObjectId | string,
    task : String,
    isDeleted ? : boolean,
    _v ?: number
}

export type IeditTask = {task : String}