"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Responses = void 0;
exports.Responses = {
    addTask: {
        Success: "task created Successfully",
        Fail: "Something went wrong task not created"
    },
    getAllTasks: {
        Fail: "Not able to get all tasks"
    },
    getTask: {
        Fail: "Task Not Found"
    },
    updateTask: {
        Success: "Task Updated SuccessFully.",
        Fail: "Task does not updated"
    },
    editTask: {
        Success: "Task edited SuccessFully.",
        Fail: "Something went wrong task not edited."
    },
    deleteTask: {
        Success: "task deleted Successfully",
        Fail: "task not Deleted"
    }
};
