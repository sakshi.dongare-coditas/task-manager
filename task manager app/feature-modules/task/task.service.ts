
import taskRepo from "./task.repo";
import { Responses } from "./task.response";
import { IeditTask, Itask } from "./task.types";

export const getAllTasks = async () =>{
    const allTask =await taskRepo.getAllTasks({isDeleted : false});
    return allTask;
    }; 

export const createTask = async (task : Itask) =>{
    const result =  await taskRepo.createTask(task);
    if(result){
        return Responses.addTask.Success;
    }else{
        throw {msg : Responses.addTask.Fail};
    }
};

export const getTask = async (taskId : string) => await taskRepo.getTask({ _id : taskId});

export const updateTask = async (taskId : string , updateTaskBody : Itask) =>{
    const result = await taskRepo.updateTask({_id : taskId},updateTaskBody)
    if(result){
        return Responses.updateTask.Success;
    }else{
        throw {msg : Responses.updateTask.Fail};
    }
};

export const editTask = async (taskId : string , editTaskBody : IeditTask) =>{
    const Task = await getTask(taskId);
    const editedTask = Task?.task+" "+editTaskBody.task;
    const result = await taskRepo.editTask({_id : taskId} , {task : editedTask});
    if(result){ 
        return Responses.editTask.Success;
    }else{
        throw {msg : Responses.editTask.Fail}
    }
}

export const deleteTask = async (taskId : string) =>{ 
    const result = await taskRepo.deleteTask({_id : taskId})
    if(result){
        return Responses.deleteTask.Success;
    }else{
        throw {msg : Responses.deleteTask.Fail};
    }
};

export default {
    getAllTasks,
    createTask,
    getTask,
    updateTask,
    editTask,
    deleteTask
}