"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.routes = void 0;
const tasks_routes_1 = __importDefault(require("../feature-modules/task/tasks.routes"));
exports.routes = [
    tasks_routes_1.default
];
