
import express from "express";
import { resisterRoutes } from "./routs/routes";
import { connectToMongo } from "./connections/mongo.connection";

export const StartServer = async () => {

    try {
        const app = express();

        resisterRoutes(app);

        await connectToMongo();
        
        const port = process.env.PORT || 3000;
        app.listen(
            port,
            () => console.log(`SERVER IS LISTNING AT PORT ${port} ...`)
        );
    } catch (err) {
        console.log("CAN NOT CONNECT TO SERVER ...");
        console.log(err);
    }
}