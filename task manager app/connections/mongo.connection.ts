
import { connect } from "mongoose";

export const connectToMongo = async () =>{
    try{
        await connect(process.env.MONGO_SECRET_KEY || "");
        console.log("CONNECTED TO MONGOOSE ...");
        return true;
    }catch(err){
        throw {message : "CAN NOT CONNECT TO MONGOOSE ..."}
    }
}