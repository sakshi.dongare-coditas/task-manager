import { Schema, model } from "mongoose";

const taskSchema = new Schema({
    task:{
        type : String,
        required : true
    },
    isDeleted : {
        type : Boolean,
        default : false
    }
});

export const taskModel = model("Task",taskSchema);